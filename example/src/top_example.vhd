library IEEE, example;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity top_example is
  generic (
    -- Global Generic Variables
    GLOBAL_DATE : std_logic_vector(31 downto 0);
    GLOBAL_TIME : std_logic_vector(31 downto 0);
    GLOBAL_VER  : std_logic_vector(31 downto 0);
    GLOBAL_SHA  : std_logic_vector(31 downto 0);
    TOP_VER     : std_logic_vector(31 downto 0);
    TOP_SHA     : std_logic_vector(31 downto 0);
    HOG_VER     : std_logic_vector(31 downto 0);
    HOG_SHA     : std_logic_vector(31 downto 0);

    --IPBus XML
    XML_SHA : std_logic_vector(31 downto 0);
    XML_VER : std_logic_vector(31 downto 0);

    -- Project Specific Lists (One for each .src file in your Top/myproj/list folder)
    MYLIB0_VER : std_logic_vector(31 downto 0);
    MYLIB0_SHA : std_logic_vector(31 downto 0);
    MYLIB1_VER : std_logic_vector(31 downto 0);
    MYLIB1_SHA : std_logic_vector(31 downto 0);

    -- Submodule Specific variables (only if you have a submodule, one per submodule)
    MYSUBMODULE0_SHA : std_logic_vector(31 downto 0);
    MYSUBMODULE1_SHA : std_logic_vector(31 downto 0);

    -- External library specific variables (only if you have an external library)
    MYEXTLIB_SHA : std_logic_vector(31 downto 0);
    -- Project flavour
    FLAVOUR      : integer;

    constant DATA_WIDTH : positive := 16;
    constant FIFO_DEPTH : positive := 256
    );
  port (
    CLK     : in  std_logic;
    RST     : in  std_logic;
    WriteEn : in  std_logic;
    DataIn  : in  std_logic_vector (DATA_WIDTH - 1 downto 0);
    ReadEn  : in  std_logic;
    DataOut : out std_logic_vector (DATA_WIDTH - 1 downto 0);
    Empty   : out std_logic;
    Full    : out std_logic
    );
end top_example;

architecture Behavioral of top_example is
  signal fifo_out : std_logic_vector(DATA_WIDTH-1 downto 0) := (others => '0');

COMPONENT fifo_generator_1
  PORT (
    clk : IN STD_LOGIC;
    srst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

begin

  fifo : fifo_generator_1
    port map (
      clk   => clk,
      srst  => RST,
      din   => DataIn,
      wr_en => WriteEn,
      rd_en => ReadEn,
      dout  => fifo_out,
      full  => Full,
      empty => Empty
      );

  adder : entity example.adder
    generic map(
      DATA_WIDTH => DATA_WIDTH
      )
    port map(
      clk     => clk,
      DataIn  => fifo_out,
      DataOut => DataOut
      );

end Behavioral;
