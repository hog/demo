### Hog example propject
Requires vivado 2019.2 or higher in your PATH

Just enter these commands:

``` console
git clone --recursive https://gitlab.cern.ch/hog/demo.git
cd demo
./Hog/CreateProject.sh example
vivado ./VivadoProject/example/example.xpr
```

Note: you can use your favourite protocol instead of https.